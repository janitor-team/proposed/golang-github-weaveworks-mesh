Source: golang-github-weaveworks-mesh
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Martín Ferrari <tincho@debian.org>,
Section: devel
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-golang (>= 1.17~),
               golang-any,
               golang-github-stretchr-testify-dev,
               golang-golang-x-crypto-dev,
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-weaveworks-mesh
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-weaveworks-mesh.git
Homepage: https://github.com/weaveworks/mesh
XS-Go-Import-Path: github.com/weaveworks/mesh

Package: golang-github-weaveworks-mesh-dev
Architecture: all
Depends: golang-github-stretchr-testify-dev,
         golang-golang-x-crypto-dev,
         ${misc:Depends},
         ${shlibs:Depends},
Description: go library to build distributed systems
 Mesh implements a gossip protocol that provide membership, unicast, and
 broadcast functionality with eventually-consistent semantics. In CAP terms, it
 is AP: highly-available and partition-tolerant.
 .
 Mesh works in a wide variety of network setups, including thru NAT and
 firewalls, and across clouds and datacenters. It works in situations where
 there is only partial connectivity, i.e. data is transparently routed across
 multiple hops when there is no direct connection between peers. It copes with
 partitions and partial network failure. It can be easily bootstrapped,
 typically only requiring knowledge of a single existing peer in the mesh to
 join. It has built-in shared-secret authentication and encryption. It scales
 to on the order of 100 peers, and has no dependencies.
